#!/usr/bin/env python3

import json
import os

import wx.adv
import wx.lib.embeddedimage

import sshmenu
import sshmenu_config


def make_icon(bitmap):
    """
    The various platforms have different requirements for the
    icon size...
    """
    img = bitmap.ConvertToImage()
    if "wxMSW" in wx.PlatformInfo:
        img = img.Scale(16, 16)
    elif "wxGTK" in wx.PlatformInfo:
        img = img.Scale(22, 22)
    # wxMac can be any size upto 128x128, so leave the source img alone....
    icon = wx.Icon(img.ConvertToBitmap())
    return icon


class SSHTaskBarIcon(wx.adv.TaskBarIcon):
    TBMENU_REFRESH = wx.NewIdRef()
    TBMENU_CONFIG = wx.NewIdRef()
    TBMENU_CLOSE = wx.NewIdRef()

    def __init__(self, parent_frame):
        wx.adv.TaskBarIcon.__init__(self)
        self.menu = None
        self.watcher = None
        self.frame = parent_frame
        self.basepath = os.path.dirname(os.path.realpath(__file__))
        self.configpath = os.path.expanduser("~/.config/ssh_menu/settings.json")
        self.configmtime = os.path.getmtime(self.configpath)

        # preload the images
        self.icons = {}
        self.icons['app'] = self.load_bitmap("utilities-terminal")
        self.icons['folder'] = self.load_bitmap(wx.ART_FOLDER)
        self.icons['session'] = self.load_bitmap("utilities-terminal")
        self.icons['refresh'] = self.load_bitmap(wx.ART_REDO)
        self.icons['config'] = self.load_bitmap("gtk-preferences")
        self.icons['close'] = self.load_bitmap(wx.ART_QUIT)

        # Set the image for the app
        icon = make_icon(self.icons['app'])
        self.SetIcon(icon, "SSHMenu")

        self.create_menu()

        # bind some events
        self.Bind(wx.adv.EVT_TASKBAR_LEFT_DCLICK, self.on_ssh_menu_activate)
        self.Bind(wx.adv.EVT_TASKBAR_RIGHT_DCLICK, self.on_ssh_menu_activate)
        self.Bind(wx.EVT_MENU, self.on_ssh_menu_refresh, id=self.TBMENU_REFRESH)
        self.Bind(wx.EVT_MENU, self.on_ssh_menu_config, id=self.TBMENU_CONFIG)
        self.Bind(wx.EVT_MENU, self.on_ssh_menu_close, id=self.TBMENU_CLOSE)

    def load_bitmap(self, art_id):
        bitmap = wx.ArtProvider.GetBitmap(art_id, wx.ART_MENU)
        if not bitmap.IsOk():
            iconpath = os.path.join(self.basepath, 'icons', f"{art_id}.png")
            bitmap = wx.Bitmap(iconpath)
        if not bitmap.IsOk():
            bitmap = wx.ArtProvider.GetBitmap(wx.ART_ERROR, wx.ART_MENU)
        return bitmap

    def create_menu(self):
        with open(self.configpath, encoding='utf-8') as json_file:
            data = json.load(json_file)

        sessions = data['sessions']
        menues = {}
        self.menu = wx.Menu()
        menues[''] = self.menu
        thing_count = 101
        for item in sessions:
            groupname = sessions[item]['group']
            if groupname in menues:
                menu = menues[groupname]
            else:
                menu = wx.Menu()
                # https://github.com/wxWidgets/Phoenix/issues/1047
                menuitem = self.menu.AppendSubMenu(menu, groupname)
                self.menu.Remove(menuitem.GetId())
                menuitem.SetBitmap(self.icons['folder'])
                self.menu.Append(menuitem)

                menues[groupname] = menu

            t1 = wx.MenuItem(menu, thing_count, item)
            t1.SetBitmap(self.icons['session'])
            menu.Append(t1)
            self.Bind(wx.EVT_MENU, self.on_ssh_menu_action, id=thing_count)
            thing_count += 1

    def CreatePopupMenu(self):
        """
        This method is called by the base class when it needs to popup
        the menu for the default EVT_RIGHT_DOWN event.
        """
        menu = wx.Menu()
        menuitem = wx.MenuItem(menu, self.TBMENU_REFRESH, 'Refresh')
        menuitem.SetBitmap(self.icons['refresh'])
        menu.Append(menuitem)
        menuitem = wx.MenuItem(menu, self.TBMENU_CONFIG, 'Config')
        menuitem.SetBitmap(self.icons['config'])
        menu.Append(menuitem)
        menuitem = wx.MenuItem(menu, self.TBMENU_CLOSE, 'Close')
        menuitem.SetBitmap(self.icons['close'])
        menu.Append(menuitem)
        return menu

    def on_ssh_menu_activate(self, _evt):
        self.PopupMenu(self.menu)

    # noinspection PyMethodMayBeStatic
    def on_ssh_menu_action(self, evt):
        id_selected = evt.GetId()
        obj = evt.GetEventObject()
        label = obj.GetLabel(id_selected)
        # print(f"connect '{label}' now...")
        sshmenu.open_session(label)

    def on_ssh_menu_refresh(self, _evt):
        self.create_menu()

    # noinspection PyMethodMayBeStatic
    def on_ssh_menu_config(self, _evt):
        frame = sshmenu_config.SSHMenuConfig(parent=None)
        frame.Show()

    def on_ssh_menu_close(self, _evt):
        wx.CallAfter(self.frame.Close)

    def add_filewatcher(self):
        self.watcher = wx.FileSystemWatcher()
        self.watcher.Bind(wx.EVT_FSWATCHER, self.on_file_change)
        config_dir = os.path.dirname(self.configpath)
        self.watcher.Add(config_dir)

    def on_file_change(self, _evt):
        configmtime = os.path.getmtime(self.configpath)
        if configmtime > self.configmtime:
            self.create_menu()
            self.configmtime = configmtime


class SshmenuService(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, title="Hello World")
        self.tbicon = SSHTaskBarIcon(self)
        self.Bind(wx.EVT_CLOSE, self.on_close_window)

    def on_close_window(self, evt):
        self.tbicon.Destroy()
        evt.Skip()

    def add_filewatcher(self):
        self.tbicon.add_filewatcher()


class SSHApp(wx.App):
    # noinspection PyPep8Naming
    def __init__(self, redirect=False, filename=None, useBestVisual=False, clearSigInt=True):
        wx.App.__init__(self, redirect, filename, useBestVisual, clearSigInt)
        self.frame = SshmenuService(None)
        # self.frame.Show(True)
        self.watcher = None
        self.MainLoop()

    def OnEventLoopEnter(self, loop):
        if loop and loop.IsMain():
            self.frame.add_filewatcher()


if __name__ == '__main__':
    app = SSHApp()
    app.MainLoop()
