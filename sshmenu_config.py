#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Subclass of MainFrame, which translate standard functions to the gui framework (wxPython)."""

import configparser
import json
import os
import re

import wx
from lxml import etree

import main_frame
import wx_helper
from sshmenu import commands


class SSHMenuConfig(main_frame.MainFrame):
    def __init__(self, parent):
        super().__init__(parent)
        self.content_changed = False
        self.config_file = None
        self.data = {"sessions": {}, "colors": {}, "misc": {}}
        self.select_session(None)
        self.select_color(None)
        self.m_choice_terminal.Clear()
        for command in commands:
            self.m_choice_terminal.Append((command))

    def close_app(self, event):
        if self.content_changed:
            if (
                wx.MessageBox(
                    "You have unsaved Data!\nDo you really want to quit?",
                    style=wx.ICON_QUESTION | wx.YES_NO | wx.NO_DEFAULT,
                )
                != wx.YES
            ):
                return
        event.Skip()

    def exit_config(self, event):
        self.Close()

    def edit_dialog(self, value):
        dlg = wx.TextEntryDialog(self, "Edit Value", "Edit", style=wx.OK | wx.CANCEL)
        dlg.SetValue(value)
        dlg.ShowModal()
        return dlg.GetValue()

    def open_config(self, event):
        with wx.FileDialog(
            self,
            "Open sshmenu settings",
            "~/.config/ssh_menu",
            wildcard="config file (*.json)|*.json",
            style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST,
        ) as fileDialog:
            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return  # the user changed their mind

            # Proceed loading the file chosen by the user
            self.config_file = fileDialog.GetPath()
            with open(self.config_file, encoding="utf-8") as json_file:
                self.data = json.load(json_file)
                self.set_gui_data()
                self.content_changed = False

    def save_config(self, event):
        if not self.config_file:
            self.save_as_config(event)
        self.regenerate_datadict()
        with open(self.config_file, "w", encoding="utf-8") as json_file:
            json.dump(self.data, json_file, indent=2)
        self.content_changed = False

    def save_as_config(self, event):
        with wx.FileDialog(
            self,
            "Save sshmenu settings",
            wildcard="config file (*.json)|*.json",
            style=wx.FD_SAVE,
        ) as fileDialog:
            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return  # the user changed their mind
            self.config_file = fileDialog.GetPath()
            self.save_config(event)

    def set_gui_data(self):
        self.m_listbox_sessions.Clear()
        self.m_listbox_colors.Clear()
        self.m_choice_colorscheme.Clear()
        if "sessions" in self.data:
            data = self.data["sessions"]
            for key in data:
                self.m_listbox_sessions.Append(key)
            self.m_listbox_sessions.Select(0)
        if "colors" in self.data:
            data = self.data["colors"]
            for key in data:
                self.m_listbox_colors.Append(key)
                self.m_choice_colorscheme.Append(key)
            self.m_listbox_colors.Select(0)
        if "misc" not in self.data:
            self.data["misc"] = {}

        self.select_session(None)
        self.select_color(None)
        self.select_misc()

    # session pane section
    def select_session(self, event):
        self.set_session_pane()
        idx = self.m_listbox_sessions.GetSelection()
        if idx == wx.NOT_FOUND:
            self.m_button_up_session.Disable()
            self.m_button_down_session.Disable()
            self.m_button_del_session.Disable()
            self.m_panelsessionsettings.Disable()
            self.m_button_copy_session.Disable()
        else:
            self.m_button_del_session.Enable()
            self.m_panelsessionsettings.Enable()
            self.m_button_copy_session.Enable()
            if idx == 0:
                self.m_button_up_session.Disable()
            else:
                self.m_button_up_session.Enable()
            if idx == self.m_listbox_sessions.GetCount() - 1:
                self.m_button_down_session.Disable()
            else:
                self.m_button_down_session.Enable()

    def set_session_pane(self):
        session = wx_helper.get_value(self.m_listbox_sessions)
        data = self.data["sessions"]
        if not data:
            return
        session_data = data[session]
        if not session_data:
            return
        wx_helper.set_value(self.m_combo_group, session_data["group"])
        wx_helper.set_value(self.m_choice_protocol, session_data["protocol"])
        wx_helper.set_value(self.m_textctrl_command, session_data["exec"])
        if session_data["protocol"] == "SSH":
            self.m_panel_ssh.Enable()
            wx_helper.set_value(self.m_textctrl_server, session_data["server"])
            wx_helper.set_value(self.m_textctrl_user, session_data["ssh_username"])
            wx_helper.set_value(self.m_textctrl_key, session_data["ssh_privatekey"])
            wx_helper.set_value(
                self.m_choice_colorscheme, session_data["ssh_color_scheme"]
            )
        else:
            self.m_panel_ssh.Disable()
            wx_helper.set_value(self.m_textctrl_server, "")
            wx_helper.set_value(self.m_textctrl_user, "")
            wx_helper.set_value(self.m_textctrl_key, "")
            wx_helper.set_value(self.m_choice_colorscheme, "")

    def apply_session(self, event):
        session = wx_helper.get_value(self.m_listbox_sessions)
        if not session:
            return
        data = self.data["sessions"]
        data2 = data[session]
        data2["group"] = wx_helper.get_value(self.m_combo_group)
        data2["protocol"] = wx_helper.get_value(self.m_choice_protocol)
        data2["exec"] = wx_helper.get_value(self.m_textctrl_command)
        if data2["protocol"] == "SSH":
            data2["server"] = wx_helper.get_value(self.m_textctrl_server)
            data2["ssh_username"] = wx_helper.get_value(self.m_textctrl_user)
            data2["ssh_privatekey"] = wx_helper.get_value(self.m_textctrl_key)
            data2["ssh_color_scheme"] = wx_helper.get_value(self.m_choice_colorscheme)
        else:
            data2["server"] = ""
            data2["ssh_username"] = ""
            data2["ssh_privatekey"] = ""
            data2["ssh_color_scheme"] = ""
        self.content_changed = True

    def insert_session(self, event):
        value = self.edit_dialog("")
        if value:
            if value in self.data["sessions"]:
                wx.MessageBox("name must be unique!", style=wx.ICON_ERROR)
                return
            idx = self.m_listbox_sessions.GetSelection() + 1
            self.m_listbox_sessions.Insert(value, idx)
            self.m_listbox_sessions.SetSelection(idx)
            data = self.data["sessions"]
            protocol = self.m_choice_protocol.GetString(0)
            data[value] = {
                "group": "",
                "protocol": protocol,
                "exec": "",
                "server": "",
                "ssh_username": "",
                "ssh_privatekey": "",
                "ssh_color_scheme": "",
            }
            self.content_changed = True
            self.select_session(None)

    def copy_session(self, event):
        name = wx_helper.get_value(self.m_listbox_sessions)
        if not name:
            return
        data = self.data["sessions"][name]
        if not data:
            return
        nameparts = list(filter(None, re.split(r"(\d+)", name)))
        if nameparts[-1].isnumeric():
            number = int(nameparts[-1])
            number += 1
            nameparts[-1] = str(number)
            name = "".join(nameparts)
        else:
            name = f"{name}1"
        value = self.edit_dialog(name)
        if value:
            if value in self.data["sessions"]:
                wx.MessageBox("name must be unique!", style=wx.ICON_ERROR)
                return
            idx = self.m_listbox_sessions.GetSelection() + 1
            self.m_listbox_sessions.Insert(value, idx)
            self.m_listbox_sessions.SetSelection(idx)
            sessions = self.data["sessions"]
            sessions[value] = {}
            data2 = sessions[value]
            for key, value in data.items():
                data2[key] = value
            self.content_changed = True
            self.select_session(None)

    def delete_session(self, event):
        session = wx_helper.get_value(self.m_listbox_sessions)
        if not session:
            return
        data = self.data["sessions"]
        data.pop(session)
        idx = self.m_listbox_sessions.FindString(session)
        self.m_listbox_sessions.Delete(idx)
        self.content_changed = True
        if (
            self.m_listbox_sessions.GetSelection() == wx.NOT_FOUND
            and self.m_listbox_sessions.GetCount()
        ):
            self.m_listbox_sessions.SetSelection(0)
        self.select_session(None)

    def down_session(self, event):
        idx = self.m_listbox_sessions.GetSelection()
        label = wx_helper.get_value(self.m_listbox_sessions)
        self.m_listbox_sessions.Delete(idx)
        idx += 1
        self.m_listbox_sessions.Insert(label, idx)
        self.m_listbox_sessions.SetSelection(idx)
        self.content_changed = True
        self.select_session(None)

    def up_session(self, event):
        idx = self.m_listbox_sessions.GetSelection()
        label = wx_helper.get_value(self.m_listbox_sessions)
        self.m_listbox_sessions.Delete(idx)
        idx -= 1
        self.m_listbox_sessions.Insert(label, idx)
        self.m_listbox_sessions.SetSelection(idx)
        self.content_changed = True
        self.select_session(None)

    def select_session_protocol(self, event):
        protocol = wx_helper.get_value(event.EventObject)
        if protocol == "SSH":
            self.m_panel_ssh.Enable()
        else:
            self.m_panel_ssh.Disable()

    # color pane section
    def select_color(self, event):
        self.set_color_pane()
        idx = self.m_listbox_colors.GetSelection()
        if idx == wx.NOT_FOUND:
            self.m_button_del_color.Disable()
            self.m_panelcolorsettings.Disable()
        else:
            self.m_button_del_color.Enable()
            self.m_panelcolorsettings.Enable()

    def set_color_pane(self):
        color = wx_helper.get_value(self.m_listbox_colors)
        data = self.data["colors"]
        if not data:
            return
        color_data = data[color]
        if not color_data:
            return
        wx_helper.set_value(self.m_colour_fg, color_data["foreground"])
        wx_helper.set_value(self.m_colour_bg, color_data["background"])

    def apply_color(self, event):
        color = wx_helper.get_value(self.m_listbox_colors)
        if not color:
            return
        data = self.data["colors"]
        data2 = data[color]
        data2["background"] = wx_helper.get_value(self.m_colour_bg)
        data2["foreground"] = wx_helper.get_value(self.m_colour_fg)
        self.content_changed = True

    def add_color(self, event):
        new_label = self.edit_dialog("")
        if new_label:
            if new_label in self.data["colors"]:
                wx.MessageBox("name must be unique!", style=wx.ICON_ERROR)
                return
            self.m_listbox_colors.Append(new_label)
            idx = self.m_listbox_colors.FindString(new_label)
            self.m_listbox_colors.SetSelection(idx)
            data = self.data["colors"]
            data[new_label] = {"background": "#000000", "foreground": "#ffffff"}
            self.content_changed = True
            self.m_choice_colorscheme.Append(new_label)
            self.content_changed = True
            self.select_color(None)

    def delete_color(self, event):
        color = wx_helper.get_value(self.m_listbox_colors)
        if not color:
            return
        data = self.data["colors"]
        data.pop(color)
        idx = self.m_listbox_colors.FindString(color)
        self.m_listbox_colors.Delete(idx)
        idx = self.m_choice_colorscheme.FindString(color)
        self.m_choice_colorscheme.Delete(idx)
        self.content_changed = True
        self.select_color(None)

    # misc pane section
    def select_misc(self):
        data = self.data["misc"]
        if not data:
            return
        wx_helper.set_value(self.m_choice_terminal, data["terminal"])

    def select_terminal_cmd(self, event):
        self.content_changed = True

    # common helpers
    def edit_value(self, event):
        listbox = event.EventObject
        self.edit_listvalue(listbox)

    def edit_listvalue(self, listbox):
        value = wx_helper.get_value(listbox)
        new_value = self.edit_dialog(value)
        if value == new_value:
            return
        idx = listbox.FindString(value)
        listbox.SetString(idx, new_value)
        if listbox == self.m_listbox_sessions:
            data = self.data["sessions"]
            data[new_value] = data.pop(value)
        elif listbox == self.m_listbox_colors:
            data = self.data["colors"]
            data[new_value] = data.pop(value)

    def regenerate_datadict(self):
        new_data = {"sessions": {}, "colors": {}, "misc": {}}
        sessions = self.data["sessions"]
        new_sessions = new_data["sessions"]
        for key in self.m_listbox_sessions.GetStrings():
            entry = sessions[key]
            new_sessions[key] = entry
        colors = self.data["colors"]
        new_colors = new_data["colors"]
        for key in self.m_listbox_colors.GetStrings():
            entry = colors[key]
            new_colors[key] = entry
        misc = new_data["misc"]
        misc["terminal"] = wx_helper.get_value(self.m_choice_terminal)
        self.data = new_data

    def generate_files(self, event):
        dirdialog = wx.DirDialog(self)
        if dirdialog.ShowModal() == wx.ID_OK:
            basedir = dirdialog.GetPath()
            self.generate_sshmenu_structure(basedir)

    # generate package section
    def generate_sshmenu_structure(self, base_dir):
        ssh_menu_dir = os.path.join(base_dir, "ssh_menu")
        self.config_file = os.path.join(ssh_menu_dir, "settings.json")
        if not os.path.isdir(ssh_menu_dir):
            os.mkdir(ssh_menu_dir)
        # cleanup sshmenu dir
        for root, dirs, files in os.walk(ssh_menu_dir, topdown=False):
            for name in files:
                if name.endswith(".desktop"):
                    os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))
        # create subfolders and create desktop files
        data = self.data["sessions"]
        for key, entry in data.items():
            dir_name = entry["group"]
            fulldir_name = os.path.join(ssh_menu_dir, dir_name)
            if dir_name:
                os.makedirs(fulldir_name, exist_ok=True)
            filename = os.path.join(fulldir_name, wx_helper.normalize_string(key))
            self.generate_desktop_file(key, filename)
        # save configfile into new dir structure
        self.save_config(None)
        # create ssh.menu (config file for the applicatiopn menu of xfce)
        self.generate_menu_file()

    @staticmethod
    def generate_desktop_file(name, filepath):
        config = configparser.ConfigParser()
        config.optionxform = lambda option: option
        config["Desktop Entry"] = {
            "Name": name,
            "Comment": "generated by sshmenu_config.py",
            "Icon": "preferences-system-network",
            "Exec": f'sshmenu.py "{name}"',
            "Terminal": "false",
            "StartupNotify": "true",
            "Type": "Application",
        }
        with open(f"{filepath}.desktop", "w", encoding="utf-8") as configfile:
            config.write(configfile, space_around_delimiters=False)

    def generate_menu_file(self):
        xml_filename = os.path.dirname(self.config_file)
        xml_filename = os.path.join(xml_filename, "ssh.menu")
        doc = etree.Element("Menu")
        etree.SubElement(doc, "Name").text = "SSH_Menu"
        etree.SubElement(doc, "AppDir").text = "."

        subdirlist = []
        data = self.data["sessions"]
        include = etree.Element("Include")
        layout = etree.Element("Layout")
        for key, entry in data.items():
            dir_name = entry["group"]
            if dir_name:
                if dir_name not in subdirlist:
                    subdirlist.append(dir_name)
                    etree.SubElement(layout, "Menuname").text = dir_name
        etree.SubElement(layout, "Separator")

        for name in subdirlist:
            submenu = self.generate_submenu(data, name)
            doc.append(submenu)

        for key, entry in data.items():
            dir_name = entry["group"]
            if not dir_name:
                etree.SubElement(
                    include, "Filename"
                ).text = f"{wx_helper.normalize_string(key)}.desktop"
                etree.SubElement(
                    layout, "Filename"
                ).text = f"{wx_helper.normalize_string(key)}.desktop"
        doc.append(include)
        doc.append(layout)

        with open(xml_filename, "wb") as f:
            f.write(
                '<?xml version="1.0" encoding="UTF-8" ?>\n'
                "<!DOCTYPE Menu\n"
                "  PUBLIC '-//freedesktop//DTD Menu 1.0//EN'\n"
                "  'https://standards.freedesktop.org/menu-spec/menu-1.0.dtd'>\n".encode(
                    "utf8"
                )
            )
            etree.ElementTree(doc).write(f, pretty_print=True)

    @staticmethod
    def generate_submenu(data, name):
        submenu = etree.Element("Menu")
        etree.SubElement(submenu, "Name").text = name
        etree.SubElement(submenu, "AppDir").text = name
        include = etree.Element("Include")
        layout = etree.Element("Layout")
        for key, entry in data.items():
            dir_name = entry["group"]
            if dir_name == name:
                etree.SubElement(
                    include, "Filename"
                ).text = f"{wx_helper.normalize_string(key)}.desktop"
                etree.SubElement(
                    layout, "Filename"
                ).text = f"{wx_helper.normalize_string(key)}.desktop"
        submenu.append(include)
        submenu.append(layout)
        return submenu


if __name__ == "__main__":
    app = wx.App()
    frame = SSHMenuConfig(parent=None)
    frame.Show()
    app.MainLoop()
