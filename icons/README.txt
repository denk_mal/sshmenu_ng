These icons are from the AdWaita Theme
with the original size of 24x24

local iconfolder       ==> adwaita file name
gtk-preferences.png    ==> preferences-system.png
wxART_QUIT.png         ==> application-exit.png
wxART_FOLDER.png       ==> folder.png
utilities-terminal.png ==> utilities-terminal.png
wxART_REDO.png         ==> view-refresh.png
