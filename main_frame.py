# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 4.2.1-0-g80c4cb6-dirty)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MainFrame
###########################################################################

class MainFrame ( wx.Frame ):

    def __init__( self, parent ):
        wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"SSHMenu Config", pos = wx.DefaultPosition, size = wx.Size( 800,600 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

        self.SetSizeHints( wx.Size( 800,600 ), wx.DefaultSize )

        b_sizer1 = wx.BoxSizer( wx.VERTICAL )

        self.m_toolBar1 = wx.ToolBar( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TB_HORIZONTAL )
        self.m_tool_open = self.m_toolBar1.AddTool( wx.ID_ANY, u"Open", wx.ArtProvider.GetBitmap( wx.ART_FILE_OPEN, wx.ART_TOOLBAR ), wx.NullBitmap, wx.ITEM_NORMAL, wx.EmptyString, u"Load", None )

        self.m_tool_save = self.m_toolBar1.AddTool( wx.ID_ANY, u"Save", wx.ArtProvider.GetBitmap( wx.ART_FILE_SAVE, wx.ART_TOOLBAR ), wx.NullBitmap, wx.ITEM_NORMAL, wx.EmptyString, u"Save", None )

        self.m_tool_save_as = self.m_toolBar1.AddTool( wx.ID_ANY, u"Save as", wx.ArtProvider.GetBitmap( wx.ART_FILE_SAVE_AS, wx.ART_TOOLBAR ), wx.NullBitmap, wx.ITEM_NORMAL, wx.EmptyString, u"Save As", None )

        self.m_toolBar1.AddSeparator()

        self.m_tool_generate = self.m_toolBar1.AddTool( wx.ID_ANY, u"Generate", wx.ArtProvider.GetBitmap( wx.ART_EXECUTABLE_FILE, wx.ART_TOOLBAR ), wx.NullBitmap, wx.ITEM_NORMAL, wx.EmptyString, u"Generate Package", None )

        self.m_toolBar1.AddSeparator()

        self.m_tool_exit = self.m_toolBar1.AddTool( wx.ID_ANY, u"Exit", wx.ArtProvider.GetBitmap( wx.ART_QUIT, wx.ART_TOOLBAR ), wx.NullBitmap, wx.ITEM_NORMAL, wx.EmptyString, u"Exit", None )

        self.m_toolBar1.Realize()

        b_sizer1.Add( self.m_toolBar1, 0, wx.EXPAND, 5 )

        self.m_notebook1 = wx.Notebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_panelsession = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        b_sizer10 = wx.BoxSizer( wx.HORIZONTAL )

        b_sizer100 = wx.BoxSizer( wx.VERTICAL )

        m_listbox_sessionsChoices = []
        self.m_listbox_sessions = wx.ListBox( self.m_panelsession, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_listbox_sessionsChoices, wx.LB_SINGLE )
        b_sizer100.Add( self.m_listbox_sessions, 1, wx.ALL|wx.EXPAND, 5 )

        b_sizer1000 = wx.BoxSizer( wx.HORIZONTAL )


        b_sizer1000.Add( ( 0, 0), 1, wx.EXPAND, 5 )

        self.m_button_up_session = wx.Button( self.m_panelsession, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.BU_EXACTFIT )

        self.m_button_up_session.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_GO_UP, wx.ART_BUTTON ) )
        self.m_button_up_session.SetToolTip( u"Move up" )

        b_sizer1000.Add( self.m_button_up_session, 0, wx.ALL, 5 )


        b_sizer1000.Add( ( 0, 0), 1, wx.EXPAND, 5 )

        self.m_button_down_session = wx.Button( self.m_panelsession, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.BU_EXACTFIT )

        self.m_button_down_session.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_GO_DOWN, wx.ART_BUTTON ) )
        self.m_button_down_session.SetToolTip( u"Move down" )

        b_sizer1000.Add( self.m_button_down_session, 0, wx.ALL, 5 )


        b_sizer1000.Add( ( 0, 0), 1, wx.EXPAND, 5 )

        self.m_button_add_session = wx.Button( self.m_panelsession, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.BU_EXACTFIT )

        self.m_button_add_session.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_NEW, wx.ART_BUTTON ) )
        self.m_button_add_session.SetToolTip( u"New item" )

        b_sizer1000.Add( self.m_button_add_session, 0, wx.ALL, 5 )


        b_sizer1000.Add( ( 0, 0), 1, wx.EXPAND, 5 )

        self.m_button_copy_session = wx.Button( self.m_panelsession, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.BU_EXACTFIT )

        self.m_button_copy_session.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_COPY, wx.ART_BUTTON ) )
        self.m_button_copy_session.SetToolTip( u"Copy item" )

        b_sizer1000.Add( self.m_button_copy_session, 0, wx.ALL, 5 )


        b_sizer1000.Add( ( 0, 0), 1, wx.EXPAND, 5 )

        self.m_button_del_session = wx.Button( self.m_panelsession, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.BU_EXACTFIT )

        self.m_button_del_session.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_DELETE, wx.ART_BUTTON ) )
        self.m_button_del_session.SetToolTip( u"Delete item" )

        b_sizer1000.Add( self.m_button_del_session, 0, wx.ALL, 5 )


        b_sizer1000.Add( ( 0, 0), 1, wx.EXPAND, 5 )


        b_sizer100.Add( b_sizer1000, 0, wx.EXPAND, 5 )


        b_sizer10.Add( b_sizer100, 2, wx.EXPAND, 5 )

        b_sizer101 = wx.BoxSizer( wx.VERTICAL )

        self.m_panelsessionsettings = wx.Panel( self.m_panelsession, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        b_sizer1010 = wx.BoxSizer( wx.VERTICAL )

        fg_sizer10100 = wx.FlexGridSizer( 0, 2, 0, 0 )
        fg_sizer10100.AddGrowableCol( 1 )
        fg_sizer10100.SetFlexibleDirection( wx.BOTH )
        fg_sizer10100.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        self.m_staticText1 = wx.StaticText( self.m_panelsessionsettings, wx.ID_ANY, u"Group:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText1.Wrap( -1 )

        fg_sizer10100.Add( self.m_staticText1, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        m_combo_groupChoices = []
        self.m_combo_group = wx.ComboBox( self.m_panelsessionsettings, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, m_combo_groupChoices, 0 )
        fg_sizer10100.Add( self.m_combo_group, 0, wx.ALL|wx.EXPAND, 5 )

        self.m_staticText2 = wx.StaticText( self.m_panelsessionsettings, wx.ID_ANY, u"Protocol:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText2.Wrap( -1 )

        fg_sizer10100.Add( self.m_staticText2, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        m_choice_protocolChoices = [ u"Terminal", u"SSH" ]
        self.m_choice_protocol = wx.Choice( self.m_panelsessionsettings, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice_protocolChoices, 0 )
        self.m_choice_protocol.SetSelection( 0 )
        fg_sizer10100.Add( self.m_choice_protocol, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_staticText11 = wx.StaticText( self.m_panelsessionsettings, wx.ID_ANY, u"Execute Command:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText11.Wrap( -1 )

        fg_sizer10100.Add( self.m_staticText11, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_textctrl_command = wx.TextCtrl( self.m_panelsessionsettings, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fg_sizer10100.Add( self.m_textctrl_command, 0, wx.ALL|wx.EXPAND, 5 )


        b_sizer1010.Add( fg_sizer10100, 0, wx.EXPAND, 5 )

        sb_sizer10101 = wx.StaticBoxSizer( wx.StaticBox( self.m_panelsessionsettings, wx.ID_ANY, u"SSH Settings" ), wx.VERTICAL )

        self.m_panel_ssh = wx.Panel( sb_sizer10101.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        fg_sizer10110 = wx.FlexGridSizer( 0, 2, 0, 0 )
        fg_sizer10110.AddGrowableCol( 1 )
        fg_sizer10110.SetFlexibleDirection( wx.BOTH )
        fg_sizer10110.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        self.m_staticText6 = wx.StaticText( self.m_panel_ssh, wx.ID_ANY, u"Server:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText6.Wrap( -1 )

        fg_sizer10110.Add( self.m_staticText6, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_textctrl_server = wx.TextCtrl( self.m_panel_ssh, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fg_sizer10110.Add( self.m_textctrl_server, 0, wx.ALL|wx.EXPAND, 5 )

        self.m_staticText7 = wx.StaticText( self.m_panel_ssh, wx.ID_ANY, u"Username:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText7.Wrap( -1 )

        fg_sizer10110.Add( self.m_staticText7, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_textctrl_user = wx.TextCtrl( self.m_panel_ssh, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fg_sizer10110.Add( self.m_textctrl_user, 0, wx.ALL|wx.EXPAND, 5 )

        self.m_staticText9 = wx.StaticText( self.m_panel_ssh, wx.ID_ANY, u"Private Key:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText9.Wrap( -1 )

        fg_sizer10110.Add( self.m_staticText9, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_textctrl_key = wx.TextCtrl( self.m_panel_ssh, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fg_sizer10110.Add( self.m_textctrl_key, 0, wx.ALL|wx.EXPAND, 5 )

        self.m_staticText10 = wx.StaticText( self.m_panel_ssh, wx.ID_ANY, u"Color Scheme:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText10.Wrap( -1 )

        fg_sizer10110.Add( self.m_staticText10, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        m_choice_colorschemeChoices = []
        self.m_choice_colorscheme = wx.Choice( self.m_panel_ssh, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice_colorschemeChoices, 0 )
        self.m_choice_colorscheme.SetSelection( 0 )
        fg_sizer10110.Add( self.m_choice_colorscheme, 0, wx.ALL, 5 )


        self.m_panel_ssh.SetSizer( fg_sizer10110 )
        self.m_panel_ssh.Layout()
        fg_sizer10110.Fit( self.m_panel_ssh )
        sb_sizer10101.Add( self.m_panel_ssh, 1, wx.EXPAND |wx.ALL, 5 )


        b_sizer1010.Add( sb_sizer10101, 1, wx.EXPAND, 5 )

        self.m_session_apply = wx.Button( self.m_panelsessionsettings, wx.ID_ANY, u"Apply", wx.DefaultPosition, wx.DefaultSize, 0 )

        self.m_session_apply.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_TICK_MARK, wx.ART_BUTTON ) )
        b_sizer1010.Add( self.m_session_apply, 0, wx.ALL|wx.ALIGN_RIGHT, 5 )


        self.m_panelsessionsettings.SetSizer( b_sizer1010 )
        self.m_panelsessionsettings.Layout()
        b_sizer1010.Fit( self.m_panelsessionsettings )
        b_sizer101.Add( self.m_panelsessionsettings, 1, wx.EXPAND |wx.ALL, 0 )


        b_sizer10.Add( b_sizer101, 5, wx.EXPAND, 5 )


        self.m_panelsession.SetSizer( b_sizer10 )
        self.m_panelsession.Layout()
        b_sizer10.Fit( self.m_panelsession )
        self.m_notebook1.AddPage( self.m_panelsession, u"Sessions", True )
        self.m_panelcolors = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        b_sizer11 = wx.BoxSizer( wx.HORIZONTAL )

        b_sizer110 = wx.BoxSizer( wx.VERTICAL )

        m_listbox_colorsChoices = []
        self.m_listbox_colors = wx.ListBox( self.m_panelcolors, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_listbox_colorsChoices, wx.LB_SINGLE|wx.LB_SORT )
        b_sizer110.Add( self.m_listbox_colors, 1, wx.ALL|wx.EXPAND, 5 )

        b_sizer1100 = wx.BoxSizer( wx.HORIZONTAL )


        b_sizer1100.Add( ( 0, 0), 1, wx.EXPAND, 5 )

        self.m_button_add_color = wx.Button( self.m_panelcolors, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.BU_EXACTFIT )

        self.m_button_add_color.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_NEW, wx.ART_BUTTON ) )
        self.m_button_add_color.SetToolTip( u"New item" )

        b_sizer1100.Add( self.m_button_add_color, 0, wx.ALL, 5 )


        b_sizer1100.Add( ( 0, 0), 1, wx.EXPAND, 5 )

        self.m_button_del_color = wx.Button( self.m_panelcolors, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.BU_EXACTFIT )

        self.m_button_del_color.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_DELETE, wx.ART_BUTTON ) )
        self.m_button_del_color.SetToolTip( u"Delete item" )

        b_sizer1100.Add( self.m_button_del_color, 0, wx.ALL, 5 )


        b_sizer1100.Add( ( 0, 0), 1, wx.EXPAND, 5 )


        b_sizer110.Add( b_sizer1100, 0, wx.EXPAND, 5 )


        b_sizer11.Add( b_sizer110, 2, wx.EXPAND, 5 )

        b_sizer111 = wx.BoxSizer( wx.VERTICAL )

        self.m_panelcolorsettings = wx.Panel( self.m_panelcolors, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        b_sizer1110 = wx.BoxSizer( wx.VERTICAL )

        fg_sizer11100 = wx.FlexGridSizer( 0, 2, 0, 0 )
        fg_sizer11100.SetFlexibleDirection( wx.BOTH )
        fg_sizer11100.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

        self.m_staticText91 = wx.StaticText( self.m_panelcolorsettings, wx.ID_ANY, u"Vordergrund", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText91.Wrap( -1 )

        fg_sizer11100.Add( self.m_staticText91, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_colour_fg = wx.ColourPickerCtrl( self.m_panelcolorsettings, wx.ID_ANY, wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNTEXT ), wx.DefaultPosition, wx.DefaultSize, wx.CLRP_DEFAULT_STYLE )
        fg_sizer11100.Add( self.m_colour_fg, 0, wx.ALL, 5 )

        self.m_staticText101 = wx.StaticText( self.m_panelcolorsettings, wx.ID_ANY, u"Hintergrund", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText101.Wrap( -1 )

        fg_sizer11100.Add( self.m_staticText101, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

        self.m_colour_bg = wx.ColourPickerCtrl( self.m_panelcolorsettings, wx.ID_ANY, wx.BLACK, wx.DefaultPosition, wx.DefaultSize, wx.CLRP_DEFAULT_STYLE )
        fg_sizer11100.Add( self.m_colour_bg, 0, wx.ALL, 5 )


        b_sizer1110.Add( fg_sizer11100, 1, wx.EXPAND, 5 )

        self.m_color_apply = wx.Button( self.m_panelcolorsettings, wx.ID_ANY, u"Apply", wx.DefaultPosition, wx.DefaultSize, 0 )

        self.m_color_apply.SetBitmap( wx.ArtProvider.GetBitmap( wx.ART_TICK_MARK, wx.ART_BUTTON ) )
        b_sizer1110.Add( self.m_color_apply, 0, wx.ALL|wx.ALIGN_RIGHT, 5 )


        self.m_panelcolorsettings.SetSizer( b_sizer1110 )
        self.m_panelcolorsettings.Layout()
        b_sizer1110.Fit( self.m_panelcolorsettings )
        b_sizer111.Add( self.m_panelcolorsettings, 1, wx.EXPAND |wx.ALL, 0 )


        b_sizer11.Add( b_sizer111, 5, wx.EXPAND, 5 )


        self.m_panelcolors.SetSizer( b_sizer11 )
        self.m_panelcolors.Layout()
        b_sizer11.Fit( self.m_panelcolors )
        self.m_notebook1.AddPage( self.m_panelcolors, u"Colorschemes", False )
        self.m_panelmisc = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
        b_sizer12 = wx.BoxSizer( wx.HORIZONTAL )

        b_sizer120 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText102 = wx.StaticText( self.m_panelmisc, wx.ID_ANY, u"Terminal App", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText102.Wrap( -1 )

        b_sizer120.Add( self.m_staticText102, 0, wx.ALL, 5 )


        b_sizer12.Add( b_sizer120, 0, wx.EXPAND, 5 )

        b_sizer121 = wx.BoxSizer( wx.VERTICAL )

        m_choice_terminalChoices = []
        self.m_choice_terminal = wx.Choice( self.m_panelmisc, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice_terminalChoices, 0 )
        self.m_choice_terminal.SetSelection( 0 )
        b_sizer121.Add( self.m_choice_terminal, 0, wx.ALL, 5 )


        b_sizer12.Add( b_sizer121, 1, wx.EXPAND, 5 )


        self.m_panelmisc.SetSizer( b_sizer12 )
        self.m_panelmisc.Layout()
        b_sizer12.Fit( self.m_panelmisc )
        self.m_notebook1.AddPage( self.m_panelmisc, u"Misc", False )

        b_sizer1.Add( self.m_notebook1, 1, wx.EXPAND |wx.ALL, 5 )


        self.SetSizer( b_sizer1 )
        self.Layout()
        self.m_statusBar1 = self.CreateStatusBar( 1, wx.STB_SIZEGRIP, wx.ID_ANY )

        self.Centre( wx.BOTH )

        # Connect Events
        self.Bind( wx.EVT_CLOSE, self.close_app )
        self.Bind( wx.EVT_TOOL, self.open_config, id = self.m_tool_open.GetId() )
        self.Bind( wx.EVT_TOOL, self.save_config, id = self.m_tool_save.GetId() )
        self.Bind( wx.EVT_TOOL, self.save_as_config, id = self.m_tool_save_as.GetId() )
        self.Bind( wx.EVT_TOOL, self.generate_files, id = self.m_tool_generate.GetId() )
        self.Bind( wx.EVT_TOOL, self.exit_config, id = self.m_tool_exit.GetId() )
        self.m_listbox_sessions.Bind( wx.EVT_LISTBOX, self.select_session )
        self.m_listbox_sessions.Bind( wx.EVT_LISTBOX_DCLICK, self.edit_value )
        self.m_button_up_session.Bind( wx.EVT_BUTTON, self.up_session )
        self.m_button_down_session.Bind( wx.EVT_BUTTON, self.down_session )
        self.m_button_add_session.Bind( wx.EVT_BUTTON, self.insert_session )
        self.m_button_copy_session.Bind( wx.EVT_BUTTON, self.copy_session )
        self.m_button_del_session.Bind( wx.EVT_BUTTON, self.delete_session )
        self.m_choice_protocol.Bind( wx.EVT_CHOICE, self.select_session_protocol )
        self.m_session_apply.Bind( wx.EVT_BUTTON, self.apply_session )
        self.m_listbox_colors.Bind( wx.EVT_LISTBOX, self.select_color )
        self.m_listbox_colors.Bind( wx.EVT_LISTBOX_DCLICK, self.edit_value )
        self.m_button_add_color.Bind( wx.EVT_BUTTON, self.add_color )
        self.m_button_del_color.Bind( wx.EVT_BUTTON, self.delete_color )
        self.m_color_apply.Bind( wx.EVT_BUTTON, self.apply_color )
        self.m_choice_terminal.Bind( wx.EVT_CHOICE, self.select_terminal_cmd )

    def __del__( self ):
        pass


    # Virtual event handlers, override them in your derived class
    def close_app( self, event ):
        event.Skip()

    def open_config( self, event ):
        event.Skip()

    def save_config( self, event ):
        event.Skip()

    def save_as_config( self, event ):
        event.Skip()

    def generate_files( self, event ):
        event.Skip()

    def exit_config( self, event ):
        event.Skip()

    def select_session( self, event ):
        event.Skip()

    def edit_value( self, event ):
        event.Skip()

    def up_session( self, event ):
        event.Skip()

    def down_session( self, event ):
        event.Skip()

    def insert_session( self, event ):
        event.Skip()

    def copy_session( self, event ):
        event.Skip()

    def delete_session( self, event ):
        event.Skip()

    def select_session_protocol( self, event ):
        event.Skip()

    def apply_session( self, event ):
        event.Skip()

    def select_color( self, event ):
        event.Skip()


    def add_color( self, event ):
        event.Skip()

    def delete_color( self, event ):
        event.Skip()

    def apply_color( self, event ):
        event.Skip()

    def select_terminal_cmd( self, event ):
        event.Skip()


