# -*- coding: utf-8 -*-

import re
import wx


def set_value(widget, value):
    """set value of a control (helper function)"""
    if isinstance(widget, wx.ColourPickerCtrl):
        set_color(widget, value)
    elif isinstance(widget, wx.Choice):
        set_menustring(widget, value)
    elif isinstance(widget, wx.RadioBox):
        set_menustring(widget, value)
    elif isinstance(widget, wx.ListBox):
        set_menustring(widget, value)
    elif isinstance(widget, wx.ComboBox):
        widget.SetValue(value)
        if widget.FindString(value) == wx.NOT_FOUND:
            widget.Append(value)
    elif isinstance(widget, wx.TextCtrl):
        widget.SetValue(value)
    elif isinstance(widget, wx.Slider):
        widget.SetValue(int(value))
    else:
        widget.SetLabel(value)


def get_value(widget):
    """get value of a control (helper function)"""
    if isinstance(widget, wx.ColourPickerCtrl):
        ret_val = get_color(widget)
    elif isinstance(widget, wx.Choice):
        ret_val = get_menustring(widget)
    elif isinstance(widget, wx.RadioBox):
        ret_val = get_menustring(widget)
    elif isinstance(widget, wx.ListBox):
        ret_val = get_menustring(widget)
    elif isinstance(widget, wx.ComboBox):
        ret_val = widget.GetValue()
    elif isinstance(widget, wx.TextCtrl):
        ret_val = widget.GetValue()
    elif isinstance(widget, wx.Slider):
        ret_val = str(widget.GetValue())
    else:
        ret_val = widget.GetLabel()
    return ret_val


def set_color(picker, color):
    """set color of a color picker from raw hex string"""
    if color:
        picker.SetColour(color)
    else:
        picker.SetColour(None)


def set_menustring(menu, name):
    """set menu selection by string (name)"""
    idx = menu.FindString(name)
    if idx == wx.NOT_FOUND:
        idx = 0
    menu.SetSelection(idx)


def get_color(picker):
    """get color of a color picker as raw hex string"""
    color = picker.GetColour()
    if color:
        return color.GetAsString(wx.C2S_HTML_SYNTAX)
    return ""


def get_menustring(menu):
    """get selected menu string (name)"""
    idx = menu.GetSelection()
    if idx == wx.NOT_FOUND:
        return ""
    return menu.GetString(menu.GetSelection())


def change_menucontent(menu, new_list):
    """replace the content of a menu"""
    menu.Clear()
    menu.AppendItems(new_list)


def normalize_string(in_string):
    out_string = in_string.lower()
    out_string = re.sub('[ .]', '_', out_string)
    out_string = re.sub('[)(:,;]', '', out_string)
    return out_string
