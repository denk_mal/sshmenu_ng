# sshmenu ng

a sshmenu replacement for the taskbar of an linux ide
using python3 (>3.6) and wxPython

### Background Color of Tabs
Background color are a good and quick way to identify and separate important sessions from unimportant sessions.

Some terminals have the ability to set the background direct with a command line parameter (like xfce4-terminal). On other terminals it is only possible to activate a preset profile (tilix, roxterm). To use indiviual background colors on those terminal sshmenu will set the hexvalue of the choosen color prefixed with the number sign as profile name to those terminals e.g. tilix --profile=#400000 if the background color in ssh_menu is set to '400000' (dark red).

The profiles that you want to use have to be created by hand before you can use them in sshmenu! It is not necessary but wise to name those profile like the background color value.

### Generate Package
This will create or update a folder containing a menu file and a substructure for use with a standard menu widget for taskbars like the standard menu from xfce4 for those that won't use sshmenu_service.py

Keep in mind, that sshmenu.py must be reachable by the exeutable path!