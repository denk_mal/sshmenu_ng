#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# pylint: disable=too-many-branches, too-many-locals, too-many-statements

import argparse
import copy
import json
import os
import pathlib
import shutil
import subprocess
import psutil

commands = {
    "roxterm":
        {"tab-cmd": "",
         "bg-color-cmd": "--profile=%s",
         "execute-cmd": ["--execute"],
         "core-cmd": ["roxterm",
                      f"--directory={pathlib.Path.home()}",
                      "--title=sshmenu",
                      "--maximize",
                      "--hide-menubar",
                      "--tab"
                      ]
         },
    "tilix":
        {"tab-cmd": "",
         "bg-color-cmd": "--profile=%s",
         "execute-cmd": "--command=",
         "core-cmd": ["tilix",
                      f"--working-directory={pathlib.Path.home()}",
                      "--title=sshmenu",
                      "--action=app-new-session",
                      "--maximize",
                      "--focus-window"
                      ]
         },
    "xfce4-terminal":
        {"tab-cmd": "--tab",
         "bg-color-cmd": "--color-bg=%s",
         "execute-cmd": "--command=",
         "core-cmd": ["xfce4-terminal",
                      f"--default-working-directory={pathlib.Path.home()}",
                      "--initial-title=sshmenu",
                      "--hide-menubar",
                      "--maximize"
                      ]
         },
}

config_file = os.path.expanduser("~/.config/ssh_menu/settings.json")


def check_if_process_running(process_name):
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if process_name.lower() in proc.name().lower():
                return proc
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return None


def get_preferred_command(prefered_term):
    # find first command that is installed (callable)
    if prefered_term in commands:
        if shutil.which(prefered_term):
            command = copy.deepcopy(commands[prefered_term]["core-cmd"])
            return command
    for key, value in commands.items():
        if shutil.which(key):
            command = copy.deepcopy(value["core-cmd"])
            return command
    return None


def read_active_sskkeys(sessions):
    keys = []
    for filename in os.listdir(os.path.expanduser("~/.ssh")):
        if filename.startswith("id_") and not filename.endswith(".pub"):
            keys.append(f"{os.path.expanduser('~/.ssh')}/{filename}")
    for sessionname in sessions:
        session = sessions[sessionname]
        if session['protocol'] == "SSH" and session['ssh_privatekey'] and session['ssh_privatekey'] not in keys:
            keys.append(session['ssh_privatekey'])

    with subprocess.Popen(['ssh-add', '-l'], stdout=subprocess.PIPE) as proc:
        rc_code = proc.wait()
        if rc_code == 0:
            registered_keys = proc.communicate()[0].splitlines()
            if len(registered_keys) == len(keys):
                return True
            rc_code = 1
        if rc_code == 1:
            keys.insert(0, 'ssh-add')
            with subprocess.Popen(keys, stdout=subprocess.PIPE) as proc:
                proc.wait()
        else:
            print("ssh-add has thrown an error!")
            return False
    return True


def parse_args(command):
    # get name of session to open
    parser = argparse.ArgumentParser(
        description=f'opens a ssh or terminal session via {command} based on sshmenu settings')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s 0.3')
    parser.add_argument('sessionname', nargs='?', default='Terminal',
                        help="the name of the sshmenu entry to startup (defaults to 'Terminal')")
    args = parser.parse_args()
    return args


def open_session(sessionname, command=None):
    with open(config_file, encoding='utf-8') as json_file:
        data = json.load(json_file)

    sessions = data['sessions']
    colors = data['colors']
    misc = {}
    term = ''
    if 'misc' in data:
        misc = data['misc']
        term = misc['terminal']

    if not command:
        command = get_preferred_command(term)
        if not command:
            print("no valid terminal command installed!")
            return

    # check if sessionname is valid (configured)
    try:
        session = sessions[sessionname]
    except KeyError:
        print(f"Sessionname '{sessionname}' not found; Choose from the following sessions:")
        index = 0
        for index, id_key in enumerate(sessions):
            index += 1
            print(f"'{id_key}'", end=" ")
            if not index % 10:
                print("")
        if index % 10:
            print("")
        return

    # if terminal is already running, oben a new tab
    if check_if_process_running(term):
        command.insert(2, commands[term]["tab-cmd"])

    # set background color for (ssh) session
    if session['ssh_color_scheme']:
        try:
            bg_color = colors[session['ssh_color_scheme']]['background']
            command.append(commands[term]["bg-color-cmd"] % bg_color)
        except KeyError:
            pass

    execute_cmd = commands[term]['execute-cmd']
    cmd_line = ""
    if session['protocol'] == "SSH":
        if not read_active_sskkeys(sessions):
            return

        server_uri = session['server']
        server_port = ""
        if ':' in server_uri:
            server_uri, server_port = server_uri.split(":")
        ssh_params = ""
        if session['ssh_username']:
            server_uri = session['ssh_username'] + '@' + server_uri
        if server_port:
            ssh_params += f" -p {server_port}"
        if session['ssh_privatekey']:
            ssh_params += " -i " + session['ssh_privatekey']
        if session['exec']:
            ssh_params += " '" + session['exec'] + "'"
        cmd_line = f"ssh -t {server_uri} {ssh_params}"
    elif session['protocol'] == "Terminal":
        if session['exec']:
            cmd_line = session['exec']

    if cmd_line:
        if isinstance(execute_cmd, list):
            command.extend(execute_cmd)
            command.append(cmd_line)
        else:
            command.append(execute_cmd + cmd_line)


    # Don't use 'with' here otherwise the app will hang here and
    # and it is impossible to open another terminal!
    # pylint: disable=consider-using-with
    subprocess.Popen(command)


def main():
    term = "tilix"
    command = get_preferred_command(term)
    if not command:
        print("no valid terminal command installed!")
        return

    args = parse_args(term)
    print(args)

    open_session(args.sessionname, command)


if __name__ == "__main__":
    main()
